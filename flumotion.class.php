<?php
/**
* @file
* This file has the class that will help to connect with flumotion public api
*
*/


/**
* General flumotion class to conect any php app with flumotion api.
*/ 

class Flumotion 
{

  /**
  * Define all the properties
  */
  private $api_key;
  private $api_nonce;
  private $api_referrer;
  private $api_timestamp;
  private $api_signature;


  /**
  * Define all the getters and setters.
  */


  /**
  * Define Api Key setter.
  * Provided API key
  *
  * @param $api_key
  * @return  string
  */
  public function setApiKey($api_key = NULL)
  {
    return $this->ApiKey = $api_key;
  }
  
  public function getApiKey()
  {
	return $this->ApiKey;
  }

  /**
  * Define Api Noce setter
  * The generated (pseudo-)random number. 
  * Generate a random 8-digit integer. The number must not be used more than once in a timespan of several minutes. 
  * Any request which uses a depleted nonce may be refused.
  * To generate the api_nonce integer take the timestamp and take out the 2 first numbers.
  *
  * @return int $api_nonce
  */
  public function setApiNonce()
  {
    $now = getdate();
    $api_nonce =  substr($now[0], 2,8);

    return $this->ApiNonce = $api_nonce;
  }
  
  public function getApiNonce()
  {
	return $this->ApiNonce;
  }

  /**
  * Define Api referrer
  * Provide Api referrer
  * Api referrer es la url desde la que se esta realizando la llamada.
  * @param $api_referrer
  * @return string 
  */
  public function setApiReferrer($api_referrer = NULL)
  {
    return $this->ApiReferrer = $api_referrer;
  }

  public function getApiReferrer()
  {
	return $this->ApiReferrer;
  }
  /**
  * Define Api Timestamp
  * The current UNIX timestamp
  *
  * @return int 
  */
  public function setApiTimestamp()
  {
    $now = getdate();
    $api_timestamp = $now[0];

    return $this->ApiTimestamp = $api_timestamp;
  }

  /**
  *  Define all the all the elements to build the url and the ApiSignature.
  * @return string with 
  */

  private function SetUrlParams()
  {
    $ApiKey = $this->getApiKey();
    $ApiNonce = $this->getApiNonce();
    $ApiReferrer = $this->getApiReferrer();
    $ApiTimestamp = $this->setApiTimestamp();

    $output = "api_key=$ApiKey&api_nonce=$ApiNonce&api_referrer=$ApiReferrer&api_timestamp=$ApiTimestamp";

    return $output;
  }

  /**
  * Define api signature
  * The result of the described process
  * We should generate the signature as the SHA-1 digest of the following string (note that the last characters of the string are the API secret):
  * api_key=ABCD&api_nonce=07476133&api_referrer=www.example.com&api_timestamp=1275404001WXYZ
  */

  private function setApiSignature()
  {

    $output = $this->SetUrlParams();
    $output = sha1($output);

    return 'api_signature=' . $output;
  }


  /**
  * Build the url to make the request.
  *
  * @return string
  */
  public function SetUrl()
  {
    $UrlPart1 = $this->SetUrlParams();
    $UrlPart2 = $this->setApiSignature();

    return $UrlPart1.$UrlPart2;
  }

  /**
  * Make the curl call
  *
  * @return a json or xml element. 
  */


}
