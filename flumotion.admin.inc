<?php

  /*
  * @file
  * This file haves the flumotion administration form, it let to save the options to call 
  * flumotion API.
  *
  */

  /**
   * Función que genera el formulario de administración para agregar los datos de flumotion
   */
  function configure_flumotion_options($form, &$form_state) {

    $form = array();

    $form['flumotion_api_key'] = array(
      '#title'    =>  t('Please add the key'),
      '#type'     => 'textfield',
      '#default_value' => variable_get('flumotion_api_key',NULL)
    );

    $form['flumotion_secret'] = array(
      '#title'    =>  t('Please add the secret fish'),
      '#type'     => 'textfield',
      '#default_value' => variable_get('flumotion_secret',NULL)
    );

    $form['flumotion_url'] = array(
      '#title'    =>  t('Please add the API url conection'),
      '#type'     => 'textfield',
      '#default_value' => variable_get('flumotion_url',NULL)
    );

    $form['flumotion_backend'] = array(
      '#title'    =>  t('Please add the Player Url'),
      '#type'     => 'textfield',
      '#default_value' => variable_get('flumotion_url',NULL)
    );
    return system_settings_form($form);
  }
