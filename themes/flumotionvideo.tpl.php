<?php
/**
 * @file
 * Template file for the flumotion default formatter
 */

/**
 *
 * Available variables:
 *
 * $values - Multidimensional Array containing both id_channel and id_video
 *
 * $settings - The settings for galleryformatter as configured for this field instance.
 */
//dpm($values);
//dpm($settings);
?>

<div> <!-- big wrapper-->
  <?php foreach ($values as $key => $value): ?>

    <div> <!-- small wrapper-->
      <iframe id="flumotion_iframe_player"
          name="flumotion_iframe_player"
          src="<?php print $player_url; ?>?player=<?php print $value['id_player']; ?>&pod=<?php print $value['id_video'];?>"
          scrolling="no" frameborder=0 width="<?php print $settings['width']; ?>px" height="<?php print $settings['height']; ?>px">
      </iframe>
    </div> <!-- end small wrapper-->
  <?php endforeach; ?>
</div> <!-- end big wrapper-->
